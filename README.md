***Portable GIS***

This is the repository for building portable GIS from the source software.

For more information see [portablegis.xyz](https://portablegis.xyz). 

For developer documentation see [portable-gis-docs.readthedocs.io/](http://portable-gis-docs.readthedocs.io/)