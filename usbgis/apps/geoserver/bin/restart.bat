@echo off
REM -----------------------------------------------------------------------------
REM Restart Script for GeoServer
REM -----------------------------------------------------------------------------


cls
echo Restarting GeoServer...
echo.

call "%~dp0\shutdown.bat"
call "%~dp0\startup.bat"

:end
  echo Geoserver restarted successfully
  echo.
