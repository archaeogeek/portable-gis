@ECHO OFF

REM Execute this file before running the GDAL, MapServer, and other commandline utilities.
REM After executing this file you should be able 
REM to run the utilities from any commandline location.

set PATH=%~dp0\Apache\cgi-bin;%~dp0\tools\gdal-ogr;%~dp0\tools\mapserv;%~dp0\tools\shapelib;%~dp0\proj\bin;%~dp0\tools\shp2tile;%~dp0\tools\shpdiff;%~dp0\tools\avce00;%~dp0\gdalbindings\python\gdal;%~dp0\tools\php;%~dp0\tools\mapcache;%~dp0\tools\berkeley-db;%~dp0\tools\sqlite;%~dp0\tools\spatialite;%~dp0\tools\unixutils;%~dp0\tools\openssl;%~dp0\tools\curl;%~dp0\tools\geotiff;%~dp0\tools\jpeg;%~dp0\tools\protobuf;%~dp0\Python;%~dp0\tools\osm2pgsql;%PATH%
echo GDAL, mapserv, Python, PHP, and commandline MS4W tools path set

set GDAL_DATA=%~dp0\gdaldata

set GDAL_DRIVER_PATH=%~dp0\gdalplugins

set PROJ_LIB=%~dp0\proj\nad

set CURL_CA_BUNDLE=%~dp0\Apache\conf\ca-bundle\cacert.pem

set SSL_CERT_FILE=%~dp0\Apache\conf\ca-bundle\cacert.pem

set OPENSSL_CONF=%~dp0\tools\openssl\openssl.cnf

:ALL_DONE
