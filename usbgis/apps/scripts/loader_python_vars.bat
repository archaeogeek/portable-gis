@ECHO OFF
TITLE Running Loader

set DRV_LTR=%~d0
set WDIR=%DRV_LTR%\usbgis\apps\ms4w


set PATH=%WDIR%\Apache\cgi-bin;%WDIR%\tools\gdal-ogr;%WDIR%\tools\mapserv;%WDIR%\tools\shapelib;%WDIR%\proj\bin;%WDIR%\tools\shp2tile;%WDIR%\tools\shpdiff;%WDIR%\tools\avce00;%WDIR%\python\gdal;%PATH%
set PYTHONPATH=%drv_ltr%\usbgis\apps\python27\App
set PYTHONHOME=%drv_ltr%\usbgis\apps\python27\App
echo paths all set
echo %PYTHONPATH%
echo %PYTHONHOME%

set GDAL_DATA=%WDIR%\gdaldata
set GDAL_DRIVER_PATH=%WDIR%\gdalplugins
set PROJ_LIB=%WDIR%\proj\nad
set CURL_CA_BUNDLE=%WDIR%\Apache\conf\ca-bundle\cacert.pem


echo read the readme.markdown file for detailed instructions on use
echo basic syntax is as follows...
echo %~d0/usbgis/apps/python27/App/python.exe loader.py loader.config

cd %~d0/usbgis/apps/loader/python

:ALL_DONE
