TITLE @echo off
TITLE Stopping PostgreSQL

%~dp0\bin\pg_ctl -D %~dp0\..\..\data\PGDATA stop
