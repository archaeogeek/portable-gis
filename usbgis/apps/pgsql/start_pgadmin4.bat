@echo off
TITLE Starting PgAdmin4
echo Please close this command window only if you wish to close PgAdmin4
echo PgAdmin4 is starting...
set PGPASSFILE=%~dp0\pgpass.conf
"%~dp0\pgadmin 4\bin\pgadmin4.exe"


if errorlevel 255 goto finish
if errorlevel 1 goto error
goto finish

:error
echo.
echo PgAdmin4 could not be started
pause

:finish
echo opening browser...
"%~dp0\..\firefox\FirefoxPortable.exe http://localhost:5436"
