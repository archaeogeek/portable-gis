@echo off
TITLE Starting PostgreSQL
echo Please do not close this command window as it will force the database server to shutdown in an unclean fashion
echo Use the Portable GIS control panel to stop PostgreSQL
echo PostgreSQL is starting...
set DRIVE_LTR=%~d0
set PGPASSFILE=%~dp0\pgpass.conf
echo pgpass variable is set to %PGPASSFILE%

%~dp0\bin\pg_ctl -D %~dp0\..\..\data\PGDATA start

if errorlevel 255 goto finish
if errorlevel 1 goto error
goto finish

:error
echo.
echo PostgreSQL could not be started
pause

:finish
