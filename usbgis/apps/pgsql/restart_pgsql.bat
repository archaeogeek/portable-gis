@echo off
TITLE Restarting PostgreSQL

%~dp0\bin\pg_ctl -D %~dp0\..\..\data\PGDATA restart
