;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;Interface Configuration

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "C:\portablegis_working_6.0\logo.bmp" ; optional
  !define MUI_ABORTWARNING
  !define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"

;--------------------------------
; Other required files

!include WinMessages.nsh
!include FileFunc.nsh
!insertmacro GetDrives
!insertmacro DriveSpace

;--------------------------------
; App-specific definitions

!define SIZE "5100" ;add here total uncompressed data size in Mb of your application
!define APPNAME "Portable GIS v6.01"
Name '${APPNAME}'
OutFile 'portablegis_setup_v60.exe'
AllowRootDirInstall true

;--------------------------------
; Pages (generic ones prefixed with insertmacro)

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "C:\portablegis_working_6.0\usbgis\docs\license.txt"
	!define MUI_LICENSEPAGE_TEXT_TOP "Scroll down to see the entire license then click to accept and continue with the installation"
!insertmacro MUI_PAGE_DIRECTORY
	!define MUI_DIRECTORYPAGE_TEXT_TOP "Select the installation directory for Portable GIS. Note that it requires at least 5.1GB of free disk space"
!insertmacro MUI_PAGE_INSTFILES
# These indented statements modify settings for MUI_PAGE_FINISH
    !define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN
    !define MUI_FINISHPAGE_RUN_CHECKED
    !define MUI_FINISHPAGE_RUN_TEXT "Start the Control Panel"
    !define MUI_FINISHPAGE_RUN_FUNCTION "LaunchLink"
    !define MUI_FINISHPAGE_SHOWREADME_CHECKED
    !define MUI_FINISHPAGE_SHOWREADME $INSTDIR\portablegis_readme.txt
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; Installer Sections

Section "primary" SecPrimary
        setOutPath $INSTDIR
		file /r usbgis
		file portablegis.exe
		file portablegis_readme.txt

		;Create uninstaller
		WriteUninstaller "$INSTDIR\Uninstall.exe"
SectionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  Delete "$INSTDIR\Uninstall.exe"
  Delete "$INSTDIR\portablegis.exe"
  Delete "$INSTDIR\portablegis_readme.txt"
  RMDir /r "$INSTDIR\usbgis"

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecPrimary ${LANG_ENGLISH} "Primary section."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecPrimary} $(DESC_SecPrimary)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;---------------------------------
;Functions

Function LaunchLink
    ExecShell "" "$INSTDIR\portablegis.exe"
FunctionEnd


