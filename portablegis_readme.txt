**************************

PORTABLE GIS

**************************

Thankyou for downloading PORTABLE GIS. This suite of programmes is distributed under the GNU General Public License Version 3. Please find a copy of the license agreement in ./usbgis/docs/license.txt

TO USE- See the full documentation pdf in ./usbgis/docs/pgis_docs.pdf, or visit https://portable-gis-docs.readthedocs.io/en/latest/index.html but if you're impatient:

1a: If using the installer, give it plenty of time to install, and ensure your installation disk has enough free space (the installer will tell you how much you need).

1b: Unzip the entire folder onto a USB stick or hard drive. This should give you the folder USBGIS and the file portablegis.exe inside your installation directory.

2: Double-click on portablegis.exe to access the control panel. Most of the functionality of PORTABLE GIS can be accessed directly from the Control Panel with no need to delve into the folders.

3: Start and/or stop programmes using the control panel to ensure they work as they are supposed to.

4: If using a USB stick, don't forget to stop all the programmes and safely eject the stick before removing it from your pc.

5: Documentation pertinent to individual programmes can be found in the relevant programme file.

WARNINGS AND SUCH LIKE:

1: This suite is designed to work only with windows, and not with any other operating system. It has been tested with Windows Windows XP professional, Windows Vista Home Premium, Windows 7 Home Premium and Windows 10. No guarantee is given that it will work with any other version of windows, although evidence that it does (or doesn't) would be gratefully received.

2: Security settings on your firewall may prevent different parts of this suite from working, in particular the web elements. The writers are not responsible for the settings on your firewall.

3: This suite is for testing purposes only. The security settings on files and databases are for ease of use and not for a production environment. The writers take no responsibility for how you deploy and use the suite.

UPGRADES AND SUPPORT:

1: Some aspects of these programmes have been adapted to allow them to work under a portable environment. Consequently upgrading by and end-user may cause them to stop working. You are advised to wait for the next release of PORTABLE GIS to get the latest versions of the software. Releases will be announced via the website at https://portablegis.xyz and by twitter @portablegis.

2: Support will be provided via email for aspects of the programme that are related to portable use and the setup as a whole, but not on the workings of the individual components, although we are happy to point you in the direction of appropriate support for individual programmes where necessary. Please visit portablegis.xyz for support.
