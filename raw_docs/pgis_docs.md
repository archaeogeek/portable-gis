# Portable GIS v6.0 Documentation

## Contents

* Included software (with versions)
* Links to websites for package-specific documentation
* How to begin using Portable GIS
* Using the Portable GIS Control Panel
* Portable GIS-Specific Instructions
    * Quantum GIS
    * GDAL/OGR
    * Python
        * Loader
    * Apache
    * PostgreSQL
    * Mapserver
    * Other utilities
* Data
* License

## Included software (with versions)

-   Quantum GIS (3.4.5)
-   PostgreSQL (10.7)
-   PostGIS (2.5)
-   MS4W (4.0.0, including Mapserver 7.4)
-   PgAdmin IV
-   Python (2.7 and 3.7)
-   Loader
-   Geoserver 2.14.4 (with java jdk 1.8)
-   GDAL Python Libraries (1.9)

The following utilities are also included:

-   Firefox (Portable app)
-   Notepad++ (Portable app)
-   Foxit PDF Reader (Portable app)

Links to websites for package-specific documentation

-   Quantum GIS:
    [http](http://www.qgis.org/)[://](http://www.qgis.org/)[www](http://www.qgis.org/)[.](http://www.qgis.org/)[qgis](http://www.qgis.org/)[.](http://www.qgis.org/)[org](http://www.qgis.org/)[/](http://www.qgis.org/)
-   PostgreSQL:
    [http](http://www.postgresql.org/)[://](http://www.postgresql.org/)[www](http://www.postgresql.org/)[.](http://www.postgresql.org/)[postgresql](http://www.postgresql.org/)[.](http://www.postgresql.org/)[org](http://www.postgresql.org/)[/](http://www.postgresql.org/)
-   PostGIS:
    [http](http://postgis.refractions.net/)[://](http://postgis.refractions.net/)[postgis](http://postgis.refractions.net/)[.](http://postgis.refractions.net/)[refractions](http://postgis.refractions.net/)[.](http://postgis.refractions.net/)[net](http://postgis.refractions.net/)[/](http://postgis.refractions.net/)
-   PgAdmin III:
    [http](http://www.pgadmin.org/)[://](http://www.pgadmin.org/)[www](http://www.pgadmin.org/)[.](http://www.pgadmin.org/)[pgadmin](http://www.pgadmin.org/)[.](http://www.pgadmin.org/)[org](http://www.pgadmin.org/)[/](http://www.pgadmin.org/)
-   MS4W:
    [http](http://www.maptools.org/ms4w)[://](http://www.maptools.org/ms4w)[www](http://www.maptools.org/ms4w)[.](http://www.maptools.org/ms4w)[maptools](http://www.maptools.org/ms4w)[.](http://www.maptools.org/ms4w)[org](http://www.maptools.org/ms4w)[/](http://www.maptools.org/ms4w)[ms](http://www.maptools.org/ms4w)[4](http://www.maptools.org/ms4w)[w](http://www.maptools.org/ms4w)
-   Python:
    [http](http://www.python.org/)[://](http://www.python.org/)[www](http://www.python.org/)[.](http://www.python.org/)[python](http://www.python.org/)[.](http://www.python.org/)[org](http://www.python.org/)
-   Loader: <http://github.com/AstunTechnology/Loader>
-   Geoserver: <http://geoserver.org/>

## How to begin using Portable GIS

If you installed the software from the installer to either your C drive
or a removable drive such as a USB stick you should see a directory
called “usbgis” and a file called “portablegis.exe”. Double-clicking on
portablegis.exe will start the Portable GIS control panel.

__Note that
if installed on a USB stick packages may take longer to load than you
are used to, due to the slower speed of the USB stick!__

While all care has been taken to minimise incompatibility with
previously installed versions of these packages, the author cannot
guarantee that problems won't occur. Portable GIS is not a “stealth”
installation where no trace will be left on your hard drive, and
similarly some programs may pick up dlls from your system, or values
from your registry.

The Control Panel gives access to the majority of the programs contained
within Portable GIS and allows you to cleanly start and stop the
server-based processes Apache and PostgreSQL. You should only start and
stop these using the control panel to ensure that they start and stop
cleanly.

Note that Apache and PostgreSQL are installed on non-default ports (88
and 5434 respectively) to try and avoid conflict with existing
installations.

##Using the Portable GIS Control Panel

Starting the Control Panel as described above opens up a menu with three
tabs:

###Welcome

This tab explains where to go to get help and training.

###Desktop Modules

This tab gives access to all the desk-top packages contained within
Portable GIS, split into sections for Desktop GIS including PgAdmin3
(remember to turn on PostgreSQL from the Web Modules tab before
starting!), Utilities, and Loader.

###Server Modules

This tab allows you to switch on the server-based components of Portable
GIS- Apache, Geoserver and PostgreSQL. Click the “start” radio buttons
to start the packages you need. Note that your firewall might try to
block these packages, but they are safe to run. Click the “stop” or
“restart” radio buttons to safely restart or close these packages down.
Portable GIS will close all the web services when the Control Panel is
closed.

Once you have started at least the Apache server, web-based components
can be accessed at [http://localhost:88](http://localhost:88/).

Geoserver can be accessed at <http://localhost:8080/geoserver>.

###Utilities

This tab gives you access to the bundled web browser, pdf reader and
text editor.

##Portable GIS-Specific Instructions

Note that these instructions relate only to the specifics of the
Portable GIS setup, and are not comprehensive instructions for using the
software. These can be found at the relevant project websites, listed
above.

###Quantum GIS

Quantum GIS can be started from the control panel Desktop Modules tab.
Quantum GIS includes the GRASS plugin, which is now part of the
processing toolbox. Additional plugins can be installed as usual via the
plugins menu.

Note that the speed at which Quantum GIS starts up is partially
determined by the number of plugins you have loaded. Disable all plugins
that are not required to see a significant improvement in load times.
Note that this information is stored in the windows registry, therefore
it is not possible to set this as part of the default installation.

###GDAL/OGR

This option opens a windows command prompt at the correct location for
accessing the ogr and gdal tools with the correct environment variables
set. Please note that these environment variables are set only for the
session initiated using the control panel menu, so the tools may not
work if accessed independently.

###Python

Python 2.7 is installed from Portable Python, and Python 3.7 is installed as part of MS4W.

If you need to set custom environment variables for your python tools, create a batch file and add it to ```[driveletter]:\usbgis\apps\scripts```. It will then be accessible from the "Python Scripts" button in the Desktop Modules tab in the control panel. By default this folder contains scripts for editing Loader config using Notepad++ and setting Loader environment variables.

####Python 2.7

The python 2.7 interpreter can be accessed from the Desktop Modules tab in
the control panel- this will start the interactive python command
prompt. For use in scripts, the python executable should be used- this
can be found at ```[driveletter]:\usbgis\apps\python27\App\python.exe```

The python 2.7 install includes easy_install for installing additional
python libraries. Please see the python documentation for usage. The
module lxml has been included as it is a requirement for Loader to run
(see below).

To use the gdal libraries, use “from osgeo import gdal” in your python
interpreter or scripts.


####Python 3.7

The python 3.7 interpreter can be accessed from the Desktop Modules tab in
the control panel- this will start the interactive python command
prompt. For use in scripts, the python executable should be used- this
can be found at ```[driveletter]:\usbgis\apps\ms4w\Python\python.exe```


####Loader

Loader is an Astun Technology tool that allows the simple loading of KML
and GML into a postgresql database, using Python. It can be found in
```[driveletter]:\usbgis\apps\loader```. It must be configured before use, using the
text-based config file, loader.config, which is found in
```..usbgis\apps\loader\python``` but can be accessed from the Desktop
Modules tab in the control panel. 

Once configured, click the "scripting CLI" button in the Desktop Modules tab. This will open a command prompt in the scripts directory, where you should pick the loader_python_vars.bat file to run, then change to the ```[driveletter]:\usbgis\apps\loader``` directory to run it.

Note that Loader will work much more quickly if a temporary working
directory is set up on the C drive rather than on a removable disk. The
location of this directory can be set using the Tools Menu -> “Set
Working Directory”. This will also set an environment variable %TMPDIR%
which can be used in the Loader config file as the temporary output
directory. The Tools Menu -> “Empty working directory” will remove __any__ files from the output directory.


###Apache

The directory that all web pages should be installed in is as follows: 
```[driveletter]:\usbgis\apps\ms4w\Apache\htdocs.```

Pages in this directory (and sub folders) will be accessible via
[http://localhost:88/](http://localhost:88/).
There is an example OpenLayers map, using data included in the
```[driveletter]:\data\OpenData``` folder at
[http://localhost:88/opendatamap.htm](http://localhost:88/opendatamap.htm)
and some information on the MS4W components at
[http://localhost:88/README_INSTALL.htm](http://localhost:88/README_INSTALL.htm).

To run cgi scripts, please place them in the folder:
```[driveletter]:\usbgis\apps\ms4w\Apache\cgi-bin```, or edit the apache
configuration files (see below) to give the server permission to execute
scripts from other folders.

The apache configuration files can be found in:
```[driveletter]:\apps\ms4w\Apache\conf```. Please note that changes to this
file may cause apache to stop working.

###PostgreSQL

PostgreSQL is set up with the following options: Default username: pgis
Password: pgis. Please create new users if you wish to use this
installation in a public environment. The data directory is
```[driveletter]:\usbgis\apps\pgsql\PGDATA,``` and the log file is in:
```[driveletter]:\usbgis\data\pg93_log```.

PostgreSQL 10.5 is set to run on the non-default port 5434. Please use
the IP address 127.0.0.1 to connectto the database on the local
PostgreSQL server if you are using Windows Vista or later.

__The postgis extensions are installed in the template database
template_postgis, so new databases must be created using that template
to be spatially enabled__. 

PostgreSQL can be administered with the tool
PgAdmin 4 from the control panel, or with the command line tools found
here: ```[driveletter]:\usbgis\apps\postgresql\bin```.

__If PGAdmin 4 fails to open a browser window, then right-click the system tray icon, choose Configure and set the following browser command:
```
../firefox/FirefoxPortable.exe %URL%
``` __.

###Geoserver

Geoserver is set up with the standard initial options, in other words
username: admin, password: admin. You may see a java warning when first
loading- please allow java to run.

###Mapserver

Mapserver 7.4 is installed in ```[driveletter]:\usbgis\apps\ms4w\Apache\cgi-bin```. It can be tested at the browser using 
[http://localhost:88/cgi-bin/mapserv.exe](http://localhost:88/cgi-bin/mapserv.exe). Called with no additional
parameters, it will display an error message.

###Other Utilities

PortableGIS contains portable versions of Firefox, a text editor called
Geany, and Foxit PDF Reader. These can be accessed from the relevant
folders of ```[driveletter]:\usbgis\apps``` but will not launch as the default
applications for web browsing, editing text or reading pdfs unless
specifically set.

### License

This product as a whole is distributed under the GNU General Public
License version 3, but it is subordinate to the License Agreements of
the constituent software packages. These may be more or less restrictive
than the GNU GPL. Please ensure that you agree with the terms of all the
licenses before using this software- these can be found in the folders
for the constituent programs.

The full license can be found at: ```[driveletter]:\usbgis\docs\license.txt```


