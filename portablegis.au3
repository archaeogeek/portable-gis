#include <GuiConstants.au3>
#NoTrayIcon
; GUI Creation
GuiCreate("Portable GIS control panel", 350, 325)
GuiSetIcon("PortableGISMenu.exe", 0)
Dim $DriveLetter = StringLeft(@ScriptDir,2)
Dim $rootDir = @ScriptDir
Dim $About = "Portable GIS version 6.0" & @CRLF  & @CRLF & "Concept: portablegis.xyz" & @CRLF & @CRLF & "GPL v3 License- see readme file for details"

; Menu Creation
$filemenu = GUICtrlCreateMenu("File")
$helpmenu = GUICtrlCreateMenu("Help")
$browsemenu = GUICtrlCreateMenuItem("Browse Drive",$filemenu)
$toolsmenu = GUICtrlCreateMenu("Tools")
$tempdiritem = GUICtrlCreateMenuItem("Set working directory", $toolsmenu)
$emptydiritem = GUICtrlCreateMenuItem("Empty working directory", $toolsmenu)
$helpitem = GUICtrlCreateMenuitem("Help Documentation (PDF)",$helpmenu)
$aboutitem = GUICtrlCreateMenuitem("About",$helpmenu)
$licenseitem = GUICtrlCreateMenuItem("View License File", $helpmenu)
$changeitem = GUICtrlCreateMenuItem("View Changelog", $helpmenu)

; Tab Creation
GUICtrlCreateTab(0,0, 350, 325)

; Tab 1 Creation
; ===================
GUICtrlCreateTabItem("Welcome")
GuiCtrlCreatePic('.\logo.jpg',96,50,157,57)
GuiCtrlCreateLabel("Welcome to Portable GIS- your windows GIS packages on a USB Stick. Please read the documentation, and enjoy using the software!", 50, 120,250, 100)



; Tab 2 Creation
; ===================
GUICtrlCreateTabItem("Desktop Modules")

GUICtrlCreateGroup("Desktop Packages", 40, 30, 270, 75)
$Button_qgis = GUICtrlCreateButton ("QGIS",  60, 60, 100)
$Button_pgadmin = GUICtrlCreateButton ("PgAdmin 4",  180, 60, 100)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

GUICtrlCreateGroup("Utilities", 40, 120, 270, 75)
$Button_GDAL = GUICtrlCreateButton ("GDAL/OGR",  60, 150, 100)
$Button_PythonScripts = GuiCtrlCreateButton("Python Scripts", 180, 150, 100)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

GuiCtrlCreateGroup("Python",40, 210, 270, 75)
$Button_Python2 = GUICtrlCreateButton("2.7 Interpreter", 60, 240, 100)
$Button_Python3 = GuiCtrlCreateButton("3.7 Interpreter",180, 240, 100)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

; Tab 3 Creation
; ===================
GUICtrlCreateTabItem("Server Modules")

GUICtrlCreateGroup ("Apache Web Server", 40, 30, 270, 75)
$radio_apache_start = GUICtrlCreateRadio ("Start", 60, 60, 60, 20)
GUICtrlSetState(-1, $GUI_ENABLE)
$radio_apache_stop = GUICtrlCreateRadio ("Stop", 120, 60, 60, 20)
GUICtrlSetState(-1, $GUI_DISABLE)
$radio_apache_restart = GUICtrlCreateRadio ("Restart", 180, 60, 60, 20)
GUICtrlSetState(-1, $GUI_DISABLE)
$apache_status=GUICtrlCreateLabel("Status: OFF", 240, 63, 70)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

GUICtrlCreateGroup ("PostgreSQL Database Server", 40, 120, 270, 75)
$radio_psql_start = GUICtrlCreateRadio ("Start", 60, 150, 60, 20)
GUICtrlSetState(-1, $GUI_ENABLE)
$radio_psql_stop = GUICtrlCreateRadio ("Stop", 120, 150, 60, 20)
GUICtrlSetState(-1, $GUI_DISABLE)
$radio_psql_restart = GUICtrlCreateRadio ("Restart", 180, 150, 60, 20)
GUICtrlSetState(-1, $GUI_DISABLE)
$psql_status=GUICtrlCreateLabel("Status: OFF", 240, 153, 70)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

GUICtrlCreateGroup ("Geoserver", 40, 210, 270, 75)
$radio_geoserver_start = GUICtrlCreateRadio ("Start", 60, 240, 60, 20)
GUICtrlSetState(-1, $GUI_ENABLE)
$radio_geoserver_stop = GUICtrlCreateRadio ("Stop", 120, 240, 60, 20)
GUICtrlSetState(-1, $GUI_DISABLE)
$radio_geoserver_restart = GUICtrlCreateRadio ("Restart", 180, 240, 60, 20)
GUICtrlSetState(-1, $GUI_DISABLE)
$geoserver_status=GUICtrlCreateLabel("Status: OFF", 240, 243, 70)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

  ; Tab 4 Creation
; ===================
GUICtrlCreateTabItem("Utilities")

GUICtrlCreateGroup("Text Editor", 40, 30, 270, 75)
$Button_notepad = GUICtrlCreateButton ("Notepad++",  120, 60, 100)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

GUICtrlCreateGroup("Web Browser", 40, 120, 270, 75)
$Button_firefox = GUICtrlCreateButton ("Firefox",  120, 150, 100)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group

GuiCtrlCreateGroup("PDF Reader",40, 210, 270, 75)
$Button_foxit = GuiCtrlCreateButton("Foxit", 120, 240, 100)
GUICtrlCreateGroup ("",-99,-99,1,1)  ;close group


; Close Tabs
GUICtrlCreateTabItem("")
; Display GUI
GuiSetState(@SW_SHOW)
; Continuous Loop to check for GUI Events
While 1
    $msg = GUIGetMsg()
    Select
		Case $msg = $browsemenu
			Run("explorer.exe " & $rootDir)
		Case $msg = $GUI_EVENT_CLOSE
			; Close web services before exiting
			If ProcessExists("httpd.exe") Then
				MsgBox(48, "Warning", "Just closing apache")
				ProcessClose("httpd.exe") ; stop apache and change status
				GUICtrlSetData($apache_status, "Status: OFF")
			EndIf
			If ProcessExists("postgres.exe") Then
				MsgBox(48, "Warning", "Just closing postgreSQL")
				Run('\usbgis\apps\psql\stop_pgsql.bat')
				GUICtrlSetData($psql_status,"Status: OFF")	; stop postgresql and change status
			EndIf


		ExitLoop
		 Case $msg = $Button_qgis
            Run('.\usbgis\apps\qgis\qgis.bat')    ; run qgis
		 Case $msg = $Button_GDAL ; open command window in correct directory for GDAL/OGR
			RunWait("cmd.exe /k .\usbgis\apps\ms4w\setenv.bat", @Workingdir, @SW_SHOW)
		 Case $msg = $Button_Python2 ; open command window in correct directory for Python 2.7
			Run(".\usbgis\apps\python27\Python-Portable.exe", @Workingdir, @SW_SHOW)
		 Case $msg = $Button_Python3 ; open command window in correct directory for Python 3.7
			Run(".\usbgis\apps\ms4w\Python\Python.exe", @Workingdir, @SW_SHOW)
		Case $msg=$Button_pgadmin ; run pgadmin 4
			If Not ProcessExists("postgres.exe") Then
			   MsgBox(48, "Warning", "PostgreSQL is not running, please start it from the server tab and try again.")
			Else
				Run('.\usbgis\apps\pgsql\start_pgadmin4.bat', "",@SW_MINIMIZE)
			EndIf
		 Case $msg = $Button_notepad
            Run('.\usbgis\apps\notepad++\Notepad++Portable.exe')    ; run notepad++
		 Case $msg = $Button_firefox
            Run('.\usbgis\apps\firefox\FirefoxPortable.exe')    ; run firefox
		 Case $msg = $Button_foxit
            Run('.\usbgis\apps\foxitreader/FoxitReaderPortable.exe')    ; run foxit
		 ;Case $msg = $Button_LoaderConfig ; Open loader.config using Notepad++
		;	Run("\usbgis\apps\notepad++\Notepad++Portable.exe \usbgis\apps\loader\python\loader.config")
		 Case $msg = $Button_PythonScripts ; open command window in loader python directory
			Run("cmd.exe /k .\usbgis\apps\python27\python_startup.bat", @Workingdir, @SW_SHOW) ; is this right- runwait means nothing else can happen until it's finished

		Case $msg = $radio_apache_start
            If ProcessExists("httpd.exe") Then
			   MsgBox(48, "Warning", "Apache is already running, and may conflict with Portable GIS. Please close it and try again.")
			 Else
			   Run('.\usbgis\apps\ms4w\apache_start.bat', "",@SW_MINIMIZE)    ; start apache
			   GUICtrlSetData($apache_status, "Status: ON")
			   GUICtrlSetState($radio_apache_start, $GUI_DISABLE)
			   GUICtrlSetState($radio_apache_stop, $GUI_ENABLE)
			   GUICtrlSetState($radio_apache_restart, $GUI_ENABLE)
			EndIf
		 Case $msg = $radio_apache_stop
			Do
			   ProcessClose("httpd.exe")    									; stop apache
			Until Not ProcessExists("httpd.exe")
			GUICtrlSetData($apache_status, "Status: OFF")
			GUICtrlSetState($radio_apache_restart, $GUI_DISABLE)
			GUICtrlSetState($radio_apache_start, $GUI_ENABLE)
			GUICtrlSetState($radio_apache_stop, $GUI_DISABLE)

		 Case $msg = $radio_apache_restart
			GUICtrlSetData($apache_status, "Status: OFF")
			GUICtrlSetState($radio_apache_start, $GUI_DISABLE)
			GUICtrlSetState($radio_apache_stop, $GUI_ENABLE)
			GUICtrlSetState($radio_apache_restart, $GUI_ENABLE)
            Do
			   ProcessClose("httpd.exe")    									; stop apache
			Until Not ProcessExists("httpd.exe")
			Run('.\usbgis\apps\ms4w\apache_start.bat', "",@SW_MINIMIZE)    ; restart apache
			GUICtrlSetData($apache_status, "Status: ON")
		Case $msg = $radio_psql_start
            If ProcessExists("postgres.exe") Then
			   MsgBox(48, "Warning", "PostgreSQL is already running, and may conflict with Portable GIS. Please close it and try again.")
			Else
			   Run('.\usbgis\apps\pgsql\start_pgsql.bat', "",@SW_MINIMIZE)    ; start postgresql
			   GUICtrlSetState($radio_psql_start, $GUI_DISABLE)
			   GUICtrlSetState($radio_psql_stop, $GUI_ENABLE)
			   GUICtrlSetState($radio_psql_restart, $GUI_ENABLE)
			   GUICtrlSetData($psql_status,"Status: ON")
			EndIf
		Case $msg = $radio_psql_stop
            Run('.\usbgis\apps\pgsql\stop_pgsql.bat', "",@SW_MINIMIZE)    ; stop postgresql
			GUICtrlSetData($psql_status,"Status: OFF")
			GUICtrlSetState($radio_psql_start, $GUI_ENABLE)
			GUICtrlSetState($radio_psql_stop, $GUI_DISABLE)
			GUICtrlSetState($radio_psql_restart, $GUI_DISABLE)
	    Case $msg = $radio_psql_restart
			Run('.\usbgis\apps\pgsql\restart_pgsql.bat',"",@SW_MINIMIZE) ; restart postgresql
			GUICtrlSetData($psql_status,"Status: OFF")
			GUICtrlSetState($radio_psql_start, $GUI_DISABLE)
			GUICtrlSetState($radio_psql_start, $GUI_ENABLE)
			GUICtrlSetState($radio_psql_restart, $GUI_ENABLE)
			GUICtrlSetData($psql_status,"Status: ON")

		Case $msg = $radio_geoserver_start
			TCPStartup()
            Local $iListenSocket=TCPListen("127.0.0.1",8080,100)
			If @error Then
				MsgBox(48, "Warning", "A process is already running on port 8080 so geoserver cannot start. Please close it and try again")
				Return False
			Else
				Run('.\usbgis\apps\geoserver\bin\startup.bat', "",@SW_MINIMIZE)    ; start geoserver
				GUICtrlSetState($radio_geoserver_start, $GUI_DISABLE)
				GUICtrlSetState($radio_geoserver_stop, $GUI_ENABLE)
				GUICtrlSetState($radio_geoserver_restart, $GUI_ENABLE)
				GUICtrlSetData($geoserver_status,"Status: ON")
			EndIf
			TCPCloseSocket($iListenSocket)
			TCPShutdown()
		Case $msg = $radio_geoserver_stop
            Run('.\usbgis\apps\geoserver\bin\shutdown.bat', "",@SW_MINIMIZE)    ; stop geoserver
			GUICtrlSetData($geoserver_status,"Status: OFF")
			GUICtrlSetState($radio_geoserver_start, $GUI_ENABLE)
			GUICtrlSetState($radio_geoserver_stop, $GUI_DISABLE)
			GUICtrlSetState($radio_geoserver_restart, $GUI_DISABLE)
	    Case $msg = $radio_geoserver_restart
			Run('.\usbgis\apps\geoserver\bin\restart.bat',"",@SW_MINIMIZE) ; restart geoserver
			GUICtrlSetData($geoserver_status,"Status: OFF")
			GUICtrlSetState($radio_psql_start, $GUI_DISABLE)
			GUICtrlSetState($radio_psql_start, $GUI_ENABLE)
			GUICtrlSetState($radio_psql_restart, $GUI_ENABLE)
			GUICtrlSetData($geoserver_status,"Status: ON")


			Case $msg = $aboutitem ; show about
				MsgBox(64,"About",$about)
			Case $msg = $helpitem; show help docs
				Run(".\usbgis\apps\foxitreader\FoxitReaderPortable.exe .\usbgis\docs\pgis_docs.pdf")
			Case $msg = $licenseitem; show license
				ShellExecute(".\usbgis\docs\license.txt", "", @WorkingDir, "open")
			Case $msg = $changeitem; show changelog
				ShellExecute(".\usbgis\docs\changelog.txt", "", @WorkingDir, "open")
			 Case $msg = $tempdiritem
				Local $var = FileSelectFolder("Choose working directory", $DriveLetter,1,$DriveLetter)
			   if $var Then
				  MsgBox(0,"Working Directory Set","Working Directory set to " & $var)
				  EnvSet("TMPDIR",$var)
			   else
				  MsgBox(0,"Working Directory Not Set","Working Directory set to " & $DriveLetter & "\tmp")
				  EnvSet("TMPDIR", $DRIVELETTER & "\tmp")
			   endif
			Case $msg = $emptydiritem
			   Local $box = MsgBox(1,"Confirm Deletion","Are you sure you want to delete all the files in the working directory?",0)
				  if $box = 1 then
					 if EnvGet("TMPDIR") Then
						FileDelete(EnvGet("TMPDIR"))
						MsgBox(0,"Deleted","All temporary files deleted",0)
					 Else
						MsgBox(0,"Notice","You don't seem to have set a working directory. Please do that and try again",0)
					 endif
				  endif
	EndSelect
Wend
